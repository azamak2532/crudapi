package com.inteliquent.crudapi.domain

import javax.persistence.*

@Entity
@Table
data class Employee(
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        var empId: Int = 0,
        var empName: String = "",
        var empSalary: Double = 0.0,
        var empDept: String = ""
)
