package com.inteliquent.crudapi

import com.inteliquent.crudapi.domain.Employee
import com.inteliquent.crudapi.repository.EmployeeRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.stereotype.Component
import java.util.*
import javax.transaction.Transactional


@SpringBootApplication
class CrudapiApplication

fun main(args: Array<String>) {
	runApplication<CrudapiApplication>(*args)
}

@Component
internal class DemoCommandLineRunner : CommandLineRunner {
	@Autowired
	lateinit var employeeRepository: EmployeeRepository

	@Throws(Exception::class)
	override fun run(vararg args: String) {
		val employee = Employee()
		employee.empName = "Jake"
		employee.empSalary = 10000.00
		employee.empDept = "IT"
		val employee1 = Employee()
		employee1.empName = "Paul"
		employee1.empSalary = 3000.00
		employee1.empDept = "HR"
		val employee2 = Employee()
		employee2.empName = "Pete"
		employee2.empSalary = 5000.00
		employee2.empDept = "Admin"
		val employee3 = Employee()
		employee3.empName = "Mahesh"
		employee3.empSalary = 9000.00
		employee3.empDept = "IT"
		employeeRepository.save(employee)
		employeeRepository.save(employee1)
		employeeRepository.save(employee2)
		employeeRepository.save(employee3)
	}
}