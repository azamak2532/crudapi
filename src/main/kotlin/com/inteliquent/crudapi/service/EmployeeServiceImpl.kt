package com.inteliquent.crudapi.service

import com.inteliquent.crudapi.domain.Employee
import com.inteliquent.crudapi.repository.EmployeeRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service
import java.util.*

@Service
class EmployeeServiceImpl : EmployeeService {
    @Autowired
    lateinit var employeeRep: EmployeeRepository

    override fun findById(id: Int): Optional<Employee> {
        return employeeRep.findById(id)
    }

    override fun findAll(): List<Employee> {
        return employeeRep.findAll()
    }

    override fun save(employee: Employee): Employee {
        return employeeRep.save(employee)
    }

    override fun update(employee: Employee): Employee {
        TODO("Not yet implemented")
    }
//
//    override fun update(id: Int): Employee {
//        return employeeRep.findById(id).map { existingEmp ->
//            val updatedEmp: Employee = existingEmp
//                    .copy(empName = newEmp.empName, empDept = newEmp.empDept, empSalary = newEmp.empSalary)
//            ResponseEntity.ok().body(employeeRepository.save(updatedEmp))
//    }
    override fun delete(id: Int) : String {
        employeeRep.deleteById(id)
        return "Record deleted"
    }
}