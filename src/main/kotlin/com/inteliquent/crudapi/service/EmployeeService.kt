package com.inteliquent.crudapi.service

import com.inteliquent.crudapi.domain.Employee
import org.springframework.stereotype.Service
import java.util.*

@Service
public interface EmployeeService {
    fun findById(id:Int): Optional<Employee>
    fun findAll(): List<Employee>
    fun save(employee: Employee): Employee
    fun update(employee: Employee): Employee
    fun delete(id: Int) : String
}