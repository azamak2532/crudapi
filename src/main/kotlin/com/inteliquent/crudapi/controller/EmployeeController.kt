package com.inteliquent.crudapi.controller

import com.inteliquent.crudapi.domain.Employee
import com.inteliquent.crudapi.repository.EmployeeRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.lang.IllegalArgumentException
import java.util.*

@RestController
class EmployeeController {

    @Autowired
    lateinit var employeeRepository: EmployeeRepository

    @GetMapping("/hello")
    fun test(): String {
        return "hello world"
    }

    @PostMapping("/saveEmployee")
    fun saveEmployee(@RequestBody employee: Employee): ResponseEntity<Employee> {
        return ResponseEntity(employeeRepository.save(employee), HttpStatus.CREATED)
    }

    @GetMapping("/getAll")
    fun getAllEmployee(): ResponseEntity<List<Employee>> {
        return ResponseEntity(employeeRepository.findAll(), HttpStatus.OK)
    }

    @GetMapping("/getEmployeeById/{id}")
    fun getEmployeeById(@PathVariable("id") id: Int): ResponseEntity<Employee> {
        return employeeRepository.findById(id).map { emp -> ResponseEntity.ok(emp) }
                .orElse(ResponseEntity(HttpStatus.NOT_FOUND))
    }

    @GetMapping("/getEmployeeBySal/{sal}")
    fun getEmployeeBySalary(@PathVariable("sal") sal: Double): ResponseEntity<List<Employee>> {
        val emps: List<Employee> = employeeRepository.findEmployeeBySalary(sal)
                ?: return ResponseEntity(HttpStatus.NOT_FOUND)
        return ResponseEntity(emps, HttpStatus.OK)
    }

    @DeleteMapping("/deleteById/{id}")
    fun deleteEmployeeById(@PathVariable("id") id: Int): ResponseEntity<String> {
        employeeRepository.deleteById(id)
        return ResponseEntity("Record Deleted", HttpStatus.OK)
    }

    @PutMapping("/updateEmployee/{id}")
    fun updateEmployee(@PathVariable("id") id: Int,
                       @RequestBody newEmp: Employee): ResponseEntity<Employee> {
        return employeeRepository.findById(id).map { existingEmp ->
            val updatedEmp: Employee = existingEmp
                    .copy(empName = newEmp.empName, empDept = newEmp.empDept, empSalary = newEmp.empSalary)
            ResponseEntity.ok().body(employeeRepository.save(updatedEmp))
        }
                .orElse(ResponseEntity.notFound().build())
//        val currentEmp = employeeRepository.findById(id)
//        val updatedEmp = employeeRepository.save(Employee(
//                empName = newEmp.empName,
//                empDept = newEmp.empDept,
//                empSalary = newEmp.empSalary
//        ))
//        return ResponseEntity.ok(updatedEmp)
    }
//    @GetMapping("/getMaxSal")
//    fun getMaxSalary(): ResponseEntity<List<Employee>> {
//        try {
//            return ResponseEntity(employeeRepository.findAllAndEmpSal(), HttpStatus.FOUND)
//        } catch (e: Exception) {
//            //val msg = "Something went wrong: " + e.message
//            return ResponseEntity(HttpStatus.NOT_FOUND)
//        }
//    }
}
