package com.inteliquent.crudapi.repository

import com.inteliquent.crudapi.domain.Employee
import org.hibernate.sql.Select
import org.springframework.context.annotation.Bean
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository

@Repository
interface EmployeeRepository : JpaRepository<Employee, Int> {
//    @Bean
//    @Query("SELECT Employee from Employee where sal > 5000")
//    fun getSal(sal: Int):Employee


    @Query(value = "SELECT * FROM employee e WHERE e.emp_salary > ?1", nativeQuery = true)
    fun findEmployeeBySalary(d:Double):List<Employee>?
}