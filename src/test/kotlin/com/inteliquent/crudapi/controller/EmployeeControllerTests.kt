//package com.inteliquent.crudapi.controller
//
//import com.inteliquent.crudapi.CrudapiApplication
//import org.junit.jupiter.api.Assertions.assertEquals
//import org.junit.jupiter.api.Assertions.assertNotNull
//import org.junit.jupiter.api.Test
//import org.junit.runner.RunWith
//import org.springframework.beans.factory.annotation.Autowired
//import org.springframework.boot.test.context.SpringBootTest
//import org.springframework.boot.test.web.client.TestRestTemplate
//import org.springframework.http.HttpStatus
//import org.springframework.test.context.junit4.SpringRunner
//
//@RunWith(SpringRunner::class)
//@SpringBootTest(classes = [CrudapiApplication::class],
//        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
//class EmployeeControllerTests {
//
//    @Autowired
//    lateinit var testRestTemplate: TestRestTemplate
//
//    @Test
//    fun whenCalled_shouldReturnHelloService() {
//        var result = testRestTemplate
//                .getForEntity("/hello-service", String::class.java)
//
//        assertNotNull(result)
//        assertEquals(result?.statusCode, HttpStatus.OK)
//        assertEquals(result?.body, "hello service")
//    }
//
//}