package com.inteliquent.crudapi

import com.inteliquent.crudapi.domain.Employee
import com.inteliquent.crudapi.repository.EmployeeRepository
import org.junit.Before
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
//import org.junit.Test
import org.springframework.boot.test.context.SpringBootTest
import java.util.*

@SpringBootTest
class CrudapiApplicationTests {

	@Autowired
	lateinit var employeeRepository: EmployeeRepository

	@Test
	fun contextLoads() {
	}

//	@Before
//	fun insertEmployeeRecords(){
//
//	}

	@Test
	fun testEmployeeInsertion() {
		val employee: Optional<Employee> = employeeRepository.findById(4)
		var employee2 = Employee()
		employee2.empId = 4
		employee2.empName = "Mahesh"
		employee2.empSalary = 9000.00
		employee2.empDept = "IT"
		assertEquals(employee, employee2, "both are not the same")
	}

}
